/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/


function numberSum(add1, add2){
	let addSum = add1 + add2;
	console.log('Displayed sum of ' + add1 + ' and ' + add2);
	console.log(addSum);
}


numberSum(5, 15);


function numberDifference(diff1, diff2){
	let minusDiff = diff1 - diff2;
	console.log('Displayed difference of ' + diff1 + ' and ' + diff2);
	console.log(minusDiff);
}

numberDifference(20, 5);

function numberProduct(prod1, prod2){
	console.log('The product of ' + prod1 + ' and ' + prod2);
	return prod1 * prod2;
}

let product = numberProduct(50, 10);
console.log(product);

function numberQuotient(div1, div2){
	console.log('The quotient of ' + div1 + ' and ' + div2);
	return div1 / div2;
}

let quotient = numberQuotient(50, 10);
console.log(quotient);

function areaCircle(radius){
	console.log('The result of getting the area of a circle ' + radius + ' radius:');
	return (3.14 * (radius ** 2));
}

let circleArea = areaCircle(15)
console.log(circleArea);

function totalAverage(num1, num2, num3, num4){
	
	let average = (num1 + num2 + num3 + num4) / 4
	console.log('The average of ' + num1 + " " + num2 + " " + num3 + " and " + num4 + ':');
	return average;
}

averageVar = totalAverage(20, 40, 60, 80);
console.log(averageVar);

function myScore(score, total){
	let myAverage = (score / total) * 100;
	console.log('Is ' + score + '/' + total + ' is a passing score?')
	let isPassed = myAverage >= 75;
	return isPassed;
} 

isPassingScore = myScore(50, 50)
console.log(isPassingScore);
